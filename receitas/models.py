from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

class Category(models.Model):
    nome = models.CharField(max_length=100)
    descricao = models.CharField(max_length=500)

    def __str__(self):
        return f'{self.nome}'

class Post(models.Model):
    nome = models.CharField(max_length=100)
    tempo_de_preparo = models.IntegerField()
    foto_url = models.URLField(max_length=1000, null=True)
    ingredientes = models.CharField(max_length=1000)
    data = models.DateTimeField(auto_now_add=True)
    categorias = models.ManyToManyField(Category)

    def __str__(self):
        return f'{self.nome} ({self.tempo_de_preparo})'

class Comment(models.Model):
    autor = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    texto = models.CharField(max_length = 500)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['-data',]

    def __str__(self):
        return f'{self.autor.username}: "{self.texto}"'
