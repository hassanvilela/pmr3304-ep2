from django.forms import ModelForm
from .models import Post, Category
from django import forms

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['nome', 'tempo_de_preparo', 'foto_url', 'ingredientes', 'categorias']
        labels = ['Nome da Receita', 'Tempo de Preparo', 'Link para a foto', 'Ingredientes', 'Categorias']
