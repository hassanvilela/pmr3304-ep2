from django.urls import path
from . import views

urlpatterns = [
    path('contact/', views.Contato.as_view(), name="contact"),
    path('receitas/<int:pk>', views.DetalheDaReceita.as_view(), name="detail"),
    path('', views.ListaDeReceitas.as_view(), name="list"),
    path('create/', views.criar_receita, name='create'),
    path('about/', views.Sobre.as_view(), name="about"),
    path('update/<int:pk>/', views.AtualizarReceita.as_view(), name='update'),
    path('delete/<int:pk>/', views.DeletarReceita.as_view(), name='delete'),
    path('search/', views.buscar_receitas, name="search"),
    path('comment/<int:post_id>', views.criar_comentario, name="comment"),
    path('categorias/', views.ListaDeCategorias.as_view(), name="categorias"),
    path('lista/<int:pk>/', views.listar_receitas, name="receitas"),
]