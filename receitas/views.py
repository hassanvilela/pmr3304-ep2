from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect 
from django.urls import reverse
from .models import Category, Comment, Post
from .forms import PostForm
from django.views import generic
from django.contrib.auth.models import User

class ListaDeReceitas(generic.ListView):
    model = Post
    template_name = 'receitas/list.html'
    context_object_name = 'lista_de_receitas'

class ListaDeCategorias(generic.ListView):
    model = Category
    template_name = 'receitas/list_cat.html'
    context_object_name = 'lista_de_categorias'

def listar_receitas(request, pk):
    list=[]
    a = Category.objects.get(id=pk)
    for post in Post.objects.all():
        if a in post.categorias.all():
            list.append(post)
    context = {"lista_2": list}
    return render(request, 'receitas/list_receitas.html', context)

class Contato(generic.TemplateView):
    template_name = 'receitas/contact.html'

def buscar_receitas(request):
    context = {}
    if request.GET.get('query', False):
        context = {"lista_de_receitas": Post.objects.filter(nome__icontains=request.GET['query'].lower())}
    return render(request, 'receitas/search.html', context)

class DetalheDaReceita(generic.DetailView):
    model = Post
    template_name = 'receitas/detail.html'

class Sobre(generic.TemplateView):
    template_name = 'receitas/about.html'

def criar_receita(request):
    if request.method == 'POST':
        post_nome = request.POST['nome']
        post_tempo_de_preparo = request.POST['tempo_de_preparo']
        post_foto_url = request.POST['foto_url']
        post_ingredientes = request.POST['ingredientes']
        post_categorias = request.POST['categorias']
        post = Post(nome=post_nome, tempo_de_preparo=post_tempo_de_preparo, foto_url=post_foto_url, ingredientes=post_ingredientes, categorias=Category.objects.get(id=post_categorias))
        post.save()
        return HttpResponseRedirect(reverse('detail', args=(post.id, )))
    else:
        form = PostForm()
        context = {'form': form}
        return render(request, 'receitas/create.html', context)

class AtualizarReceita(generic.UpdateView):
    model = Post
    fields = ['nome', 'tempo_de_preparo', 'foto_url', 'ingredientes']
    template_name = 'receitas/update.html'
    def get_success_url(self):
        return reverse('detail', kwargs={'pk': self.object.pk})

class DeletarReceita(generic.DeleteView):
    model = Post
    template_name = 'receitas/delete.html'
    def get_success_url(self):
        return reverse('list')

def criar_comentario(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        comentario_autor = User.objects.get_or_create(username=request.POST['autor'])[0]
        comentario_texto = request.POST['texto']
        comentario = Comment(autor=comentario_autor, texto=comentario_texto, post=post)
        comentario.save()
        return HttpResponseRedirect(
            reverse('detail', args=(post_id, )))
    else:
        context = { 'post': post }
        return render(request, 'receitas/comment.html', context)